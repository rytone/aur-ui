import Browser
import Browser.Navigation as Nav
import Html exposing (Html)
import Html.Attributes as Attr
import Html.Events as Events
import Http
import Json.Decode as Json
import Time
import Time.Format as TimeFormat
import Url exposing (Url)
import Url.Parser as UrlParse exposing (Parser, (</>), s)

-- setup
main = Browser.application
    { init = init
    , view = view
    , update = update
    , subscriptions = \_ -> Sub.none
    , onUrlChange = \_ -> Nop
    , onUrlRequest = \r -> ClickedLink r
    }

-- model
type alias RustTime =
    { secs: Int
    , nanos: Int
    }

type alias Package =
    { name: String
    , version: String
    , description: String
    }

type alias Repo =
    { name: String
    , packages: List Package
    , updated: RustTime
    }

type Model
    = Uninitialized
    | NotFound
    | ShowRepo Repo String

type Msg
    = FetchRepo String
    | GotRepo (Result Http.Error Repo)
    | UpdateFilter String
    | ClickedLink Browser.UrlRequest
    | Nop

-- parsing
rustTimeDecoder: Json.Decoder RustTime
rustTimeDecoder = 
    Json.map2 RustTime
        (Json.field "secs_since_epoch" Json.int)
        (Json.field "nanos_since_epoch" Json.int)

packageDecoder : Json.Decoder Package
packageDecoder =
    Json.map3 Package
        (Json.field "n" Json.string)
        (Json.field "v" Json.string)
        (Json.field "d" Json.string)

packagesDecoder : Json.Decoder (List Package)
packagesDecoder = Json.list packageDecoder

repoDecoder =
    Json.map3 Repo
        (Json.field "r" Json.string)
        (Json.field "p" packagesDecoder)
        (Json.field "u" rustTimeDecoder)

routeParser : Parser (String -> a) a
routeParser = s "aur" </> s "browse" </> UrlParse.string

-- http
getRepo : String -> Cmd Msg
getRepo repo = Http.send GotRepo (Http.get ("/aur/list/" ++ repo) repoDecoder)

-- init
init: Json.Value -> Url -> Nav.Key -> (Model, Cmd Msg)
init flags url _ = 
    case Json.decodeValue repoDecoder flags of
        Ok repo ->
            ( ShowRepo repo ""
            , Cmd.none
            )
        Err _ ->
            case (UrlParse.parse routeParser url) of
                Just repo ->
                    ( Uninitialized
                    , getRepo repo
                    )
                Nothing ->
                    ( Uninitialized
                    , Cmd.none
                    )

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        FetchRepo repo ->
            ( model
            , getRepo repo
            )
        GotRepo result ->
            case result of
                Ok repo ->
                    ( ShowRepo repo ""
                    , Cmd.none
                    )
                Err _ ->
                    ( NotFound
                    , Cmd.none
                    )
        UpdateFilter f ->
            case model of
                Uninitialized -> (model, Cmd.none)
                NotFound -> (model, Cmd.none)
                ShowRepo repo filter -> (ShowRepo repo f, Cmd.none)
        ClickedLink r ->
            case r of
                Browser.Internal url -> (model, Nav.load (Url.toString url))
                Browser.External url -> (model, Nav.load url)
        Nop -> (model, Cmd.none)

-- view
formatDate : Int -> String
formatDate time = TimeFormat.format Time.utc "Weekday, Month Day @ padHour:padMinute UTC" time

renderBlank : List (Html Msg)
renderBlank = []

renderNotFound : List (Html Msg)
renderNotFound = [ Html.h1 [] [ Html.text "Repo not found" ] ]

renderPackage : Package -> Html Msg
renderPackage package =
    Html.div [ Attr.class "package" ]
        [ Html.h2 [] 
            [ Html.a [ Attr.href ("https://aur.archlinux.org/packages/" ++ package.name) ] [ Html.text package.name ]
            , Html.span [ Attr.class "version" ] [ Html.text package.version ]
            ]
        , Html.text package.description
        ]

renderRepo : Repo -> String -> List (Html Msg)
renderRepo repo filter = 
    let
        filteredPackages = List.filter (\p -> String.contains filter p.name) repo.packages
    in
    [ Html.h1 [] [ Html.text repo.name ]
    , Html.input
        [ Attr.class "search"
        , Attr.placeholder "Search packages..."
        , Events.onInput UpdateFilter
        ]
        []
    ] ++ List.map renderPackage filteredPackages

render : Model -> List (Html Msg)
render model = case model of
    Uninitialized -> renderBlank
    NotFound -> renderNotFound
    ShowRepo repo filter -> renderRepo repo filter

title : Model -> String
title model = case model of
    Uninitialized -> "aur"
    NotFound -> "repo not found"
    ShowRepo repo filter -> repo.name

sidebar: Model -> List (Html Msg)
sidebar model = case model of
    Uninitialized -> renderBlank
    NotFound -> [ Html.a [ Attr.href "/aur", Attr.class "home-link" ] [ Html.text "<- back to aur home" ] ]
    ShowRepo repo _ ->
        [ Html.text "Last updated "
        , Html.b [] [ Html.text (formatDate (repo.updated.secs * 1000)) ]
        ]

view : Model -> Browser.Document Msg
view model =
    {
        title = title model,
        body = 
            [ Html.div
                [ Attr.class "fullwidth"
                , Attr.class "flex"
                , Attr.class "center-x"
                , Attr.class "collapse-mobile"
                ]
                [ Html.div [ Attr.class "article" ] (render model)
                , Html.div [ Attr.class "sidebar" ] (sidebar model)
                ]
            ]
    }
